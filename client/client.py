import numpy as np
import os
import sys
from parse_query import parse_input
import webbrowser
import os.path
import scipy.sparse
import scipy.sparse.linalg
from itertools import izip
from math import sqrt
import numpy.linalg
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--idf", action="store_true", help="enable inverse document frequency")
parser.add_argument("-s", "--svd", type=int, help="number of svd numbers")
args = parser.parse_args()

ARTICLES = 8

# print args.svd
# print args.idf

K = args.svd

if args.idf:
    svd_name = 'resources/V_svd' + str(K) + '_idf' + '.npy'
    svd_normalized_name = 'resources/V_svd_normalized' + str(K) + '_idf' + '.npy'
    v_name = 'resources/V_idf.npy'
    v_norm_name = 'resources/V_normalized_idf.npy'

else:
    svd_name = 'resources/V_svd' + str(K) + '.npy'
    svd_normalized_name = 'resources/V_svd_normalized' + str(K) + '.npy'
    v_name = 'resources/V.npy'
    v_norm_name = 'resources/V_normalized.npy'

# if len(sys.argv) > 2:
#     K = int(sys.argv[2])



def get_matrix(file_name):
    X = np.load('resources/X.npy')
    Y = np.load('resources/Y.npy')
    W = np.load(file_name)

    return scipy.sparse.coo_matrix((W, (X, Y)), shape=(77720, 4000))


def prepare_normalized_matrix():
    print 'preparing normalized matrix...'

    X = np.load('resources/X.npy')
    Y = np.load('resources/Y.npy')
    W = np.load(v_name)

    A = scipy.sparse.coo_matrix((W, (X, Y)), shape=(77720, 4000)).tocsc()

    L = np.zeros(4000)

    for i in xrange(4000):
        L[i] = np.linalg.norm(A.getcol(i).todense())

    for i, y in enumerate(Y):
        W[i] /= L[y]

    np.save(v_norm_name, W)


def prepare_svd_matrix():
    print 'preparing svd matrix...'

    A = get_matrix(v_name)

    U, S, VT = scipy.sparse.linalg.svds(A, K)
    A = np.dot(U[:, 0:K], np.dot(np.diag(S[0:K]), VT[0:K, :]))

    print 'saving SVD matrix...'

    X = np.load('resources/X.npy')
    Y = np.load('resources/Y.npy')
    W = np.zeros(len(X), dtype=np.float32)

    # saving
    for i, (x, y) in enumerate(izip(X, Y)):
        W[i] = A[x, y]

    np.save(svd_name, W)

def prepare_svd_normalized():
    print 'preparing normalized svd matrix...'

    X = np.load('resources/X.npy')
    Y = np.load('resources/Y.npy')
    W = np.load(svd_name)

    A = scipy.sparse.coo_matrix((W, (X, Y)), shape=(77720, 4000)).tocsc()

    L = np.zeros(4000)

    for i in xrange(4000):
        L[i] = np.linalg.norm(A.getcol(i).todense())

    for i, y in enumerate(Y):
        W[i] /= L[y]

    np.save(svd_normalized_name, W)


def prepare():
    """
        Returns: A, get_cosinus, get_len, urls
    """
    ################### URLS ##########################
    
    urls = []
    with open('resources/urls.txt', 'r') as urls_file:
        for line in urls_file:
            url, title = line.split('|')
            urls.append((url, title[:-1])) 

    ################## MATRIXES #######################

    
    if args.svd:

        if not os.path.isfile(svd_normalized_name):

            if not os.path.isfile(svd_name):
                prepare_svd_matrix()

            prepare_svd_normalized()

            os.remove(svd_name)

        loaded_matrix = svd_normalized_name
        matrix = get_matrix(svd_normalized_name)

    else:

        if not os.path.isfile(v_norm_name):
            prepare_normalized_matrix()

        loaded_matrix = v_norm_name
        matrix = get_matrix(v_norm_name)
   
    return matrix, urls, loaded_matrix


def prepare_result(cosinuses):

    sorted_cos = sorted(zip(range(len(cosinuses)), cosinuses), key=lambda x: x[1], reverse=True)

    with open('result.html', 'w') as file:
        with_str = 'with' if args.svd else 'without'
        k_str = ' K=' + str(K) if args.svd else ''
        idf_str = ' with IDF' if args.idf else ' without IDF'

        file.write('<h2>First ' + str(ARTICLES) + ' results of searching ' + with_str + ' SVD' + k_str + idf_str + '</h2>')
        file.write('<h3>Query: ' + input_line + '</h3>')
        file.write('<ul style="list-style-type:decimal-leading-zero">')

        for i, (index, number) in enumerate(sorted_cos[:ARTICLES]):
            file.write('<li><a href=\"' + urls[index][0] + '\">' + urls[index][1] + '</a> &#9;' + str(sorted_cos[i][1]) + '</li>' + '\n')

        file.write('</ul>')

    webbrowser.open_new('result.html')

    print 'opening result...'

####################################### SCIRPT #######################################
######################################################################################
######################################################################################

print 'preparing matrixes...'
A, urls, loaded_matrix = prepare()

print 'transposing matrix A...'
A = A.transpose()

print 'loaded matrix ' + loaded_matrix + '...'

while True:
    input_line = raw_input("\nPlease specify query:\n")

    if input_line == 'q':
        break

    q = parse_input(input_line)
    q /= np.linalg.norm(q)

    cosinuses = A.dot(q)

    print 'preparing result...'
    prepare_result(cosinuses)