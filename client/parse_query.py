import re
import numpy as np

buck_of_words = set()
words_indexes = dict()

# Mamy indeksy slowa
with open('resources/words.txt') as words_file:
    for i, line in enumerate(words_file):
        buck_of_words.add(line[:-1])
        words_indexes[line[:-1]] = i


EXCLUDED_WORDS = {'and', 'or', 'of', 'an', 'its', 'it', 'at', 'the', 'is', 'on', 'to', 'be', 'to',
         'for', 'do', '</doc>', 'but', 'as', 'by', 'in'}

NIELICZBY_REGEX = re.compile("\w*\d\w*")
ALFANUMERYCZNE_REGEX = re.compile("[^\w\s]")


def extract_words_from_line(line):
    """
        male litery, tylko alfanumeryczne znaki, nie EXCLUDED_WORDS, dluzsze niz 1 znak,
        nie slowa-myslniki
    """
    extracted_words = []

    line = re.sub("-", " ", line)
    line = re.sub("_", " ", line)
    line = NIELICZBY_REGEX.sub(" ", line)
    line = ALFANUMERYCZNE_REGEX.sub(" ", line).lower()

    for word in line.split():
        if len(word) > 1 and (word not in EXCLUDED_WORDS):
            extracted_words.append(word)

    return extracted_words

def parse_input(input):
    words_from_input = filter(lambda t: t in buck_of_words, extract_words_from_line(input))
    input_vector = np.zeros(77720, dtype=np.float32)

    for word in words_from_input:
        input_vector[words_indexes[word]] += 1

    return input_vector