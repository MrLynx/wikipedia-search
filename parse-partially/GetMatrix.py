from scipy.sparse import dok_matrix
import numpy as np

X = []
Y = []
V = []

with open('out1000/vectors.txt', 'r') as vectors_file:
    for k, line in enumerate(vectors_file):
        i, j, x = line.split(':')
        i, j, x = int(i), int(j), float(x)

        print j
        X.append(i)
        Y.append(j)
        V.append(x)

X = np.array(X, dtype=np.int)
Y = np.array(Y, dtype=np.int)
V = np.array(V, dtype=np.float32)

np.save('out1000/x.npy', X)
np.save('out1000/y.npy', Y)
np.save('out1000/v.npy', V)