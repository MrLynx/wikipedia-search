import time

all_words = set()

with open('out1000/lines1000.txt', 'r') as input_file, open('out1000/words1000.txt', 'w') as output_file:
    for i, line in enumerate(input_file):

        if i % 2 == 1:
            all_words.update(line.split())

    for word in all_words:
        output_file.write(word + "\n")
