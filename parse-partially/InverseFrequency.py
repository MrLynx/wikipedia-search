import numpy as np
import scipy.sparse


def get_articles(file_name):
    with open(file_name) as file:
        for i, line in enumerate(file):

            if i % 2 == 0:
                id = i / 2
                _, url, name = tuple(line.split('|'))

            else:
                yield id, url, name[:-1], line.split()


lista = []
occurences = dict()
index_of_words = dict()

with open('out1000/words1000.txt', 'r') as words_file:
    for i, line in enumerate(words_file):
        occurences[line[:-1]] = 0
        index_of_words[line[:-1]] = i


for i, (id, url, name, words) in enumerate(get_articles('out1000/lines1000.txt')):

    for word in set(words):
        occurences[word] += 1


X = np.load('out1000/x.npy')
Y = np.load('out1000/y.npy')
V = np.load('out1000/v.npy')

matrix = scipy.sparse.dok_matrix((77720, 4000))

for i in xrange(77720):
    matrix[X[i], Y[i]] = V[i]

for j, word in enumerate(index_of_words):
    k = index_of_words[word]
    o = occurences[word]
    print j * 1.0 / 77720

    for i in xrange(4000):
        if matrix[k, i] > 0:
            matrix[k, i] *= np.log10(4000.0/o)

for i in xrange(77720):
    print i
    x = X[i]
    y = Y[i]
    V[i] = matrix[x, y]

print matrix
np.save('out1000/vidf.npy', V)
