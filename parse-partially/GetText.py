import time
import re

NUMBER_OF_ARTICLES = 4335341

with open("resources/wikipedia-lines.txt", "r") as read_file, open("out1000/lines1000.txt", "w") as write_file:
    articles = 0

    for i, line in enumerate(read_file):
        print i

        if i % 1000 == 0:
            write_file.write(line)

        if (i - 1) % 1000 == 0:
            write_file.write(line)
            articles += 1

        if articles == 4000:
            break