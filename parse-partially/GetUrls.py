
def get_articles(file_name):
    with open(file_name) as file:
        for i, line in enumerate(file):

            if i % 2 == 0:
                id = i / 2
                _, url, name = tuple(line.split('|'))

            else:
                yield id, url, name[:-1], line.split()


with open('out1000/urls.txt', 'w') as vectors_file:
    # ITERATE OVER ARTICLES
    for i, (id, url, name, words) in enumerate(get_articles('out1000/lines1000.txt')):
        vectors_file.write(url + "|" + name + '\n')