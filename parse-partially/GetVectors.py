
def get_articles(file_name):
    with open(file_name) as file:
        for i, line in enumerate(file):

            if i % 2 == 0:
                id = i / 2
                _, url, name = tuple(line.split('|'))

            else:
                yield id, url, name[:-1], line.split()

words_indexes = dict()
buck_of_words = set()


# Mamy indeksy slowa
with open('out1000/words1000.txt') as words_file:
    for i, line in enumerate(words_file):
        buck_of_words.add(line[:-1])
        words_indexes[line[:-1]] = i


with open('out1000/vectors.txt', 'w') as vectors_file:
    # ITERATE OVER ARTICLES
    for i, (id, url, name, words) in enumerate(get_articles('out1000/lines1000.txt')):
        article_dict = dict()

        # GET NUMBER OF OCCURENCES
        for word in words:
            if word in buck_of_words:
                if word not in article_dict:
                    article_dict[word] = 0

                article_dict[word] += 1

        for word in article_dict:
            vectors_file.write(str(words_indexes[word]) + ':' + str(id) + ':' + str(article_dict[word]) + '\n')


print str(words_indexes['spiders'])