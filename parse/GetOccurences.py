import time
import re

NUMBER_OF_ARTICLES = 4335341


def get_articles(file_name):
    with open(file_name) as file:
        for i, line in enumerate(file):

            if i % 2 == 0:
                id, url, name = tuple(line.split('|'))

            else:
                yield id, url, name[:-1], line.split()


def print_time(start_time, articles_processed):
    difftime = time.time() - start_time
    fulltime = difftime * (1.0 * NUMBER_OF_ARTICLES / articles_processed)
    estimated_time = fulltime - difftime
    print '%d Estimated time: %dh %dm %ds' % (articles_processed, estimated_time / 3600, (estimated_time % 3600) / 60, estimated_time % 60)


def get_occurences():

    words_dict = dict()
    index_of_words = dict()
    with open('resources/words.txt', 'r') as words_file:
        for i, line in enumerate(words_file):
            word = line[:-1]
            words_dict[word] = 0
            index_of_words[word[:-1]] = i

    start_time = time.time()
    for i, (id, url, name, words) in enumerate(get_articles('resources/wikipedia-lines.txt')):

        if i % 1000 == 0 and i != 0:
            print_time(start_time, i)

        for word in words:
            words_dict[word] += 1


    with open('out/words-occurences.txt', 'w') as file:
        for word in words_dict:
            file.write(str(index_of_words[word]) + ' ' + words_dict[word] + '\n')



get_occurences()