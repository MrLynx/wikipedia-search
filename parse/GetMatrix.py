import time

NUMBER_OF_ARTICLES = 4335341

def get_articles(file_name):
	with open(file_name) as file:
		for i, line in enumerate(file):

			if i % 2 == 0:
				id, url, name = tuple(line.split('|'))

			else:
				yield id, url, name[:-1], line.split()


def print_time(start_time, articles_processed):
    difftime = time.time() - start_time
    fulltime = difftime * (1.0 * NUMBER_OF_ARTICLES / articles_processed)
    estimated_time = fulltime - difftime
    print '%d Estimated time: %dh %dm %ds' % (articles_processed, estimated_time / 3600, (estimated_time % 3600) / 60, estimated_time % 60)


def get_matrix():
	index_of_words = dict()
	buck_of_words = set()
	with open('resources/words.txt') as words_file:
		for i, word in enumerate(words_file):
			buck_of_words.add(word[:-1])
			index_of_words[word[:-1]] = i

	start_time = time.time()
	with open('out/matrix.mtx', 'w') as vectors_file:
		# ITERATE OVER ARTICLES
		for i, (id, url, name, words) in enumerate(get_articles('resources/wikipedia-lines.txt')):
			article_dict = dict()

			if i % 1000 == 0 and i != 0:
				print_time(start_time, i)

			# GET NUMBER OF OCCURENCES
			for word in words:
				if word in buck_of_words:
					if word not in article_dict:
						article_dict[word] = 0

					article_dict[word] += 1

			for word in article_dict:
				vectors_file.write(str(index_of_words[word]) + ' ' + id + ' ' + str(article_dict[word]) + ' ')
				vectors_file.write('\n')

get_matrix()