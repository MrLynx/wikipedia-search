import time

NUMBER_OF_ARTICLES = 4335341


def print_time(start_time, articles_processed):
    difftime = time.time() - start_time
    fulltime = difftime * (1.0 * NUMBER_OF_ARTICLES / articles_processed)
    estimated_time = fulltime - difftime
    print '%d Estimated time: %dh %dm %ds' % (articles_processed, estimated_time / 3600, (estimated_time % 3600) / 60, estimated_time % 60)


def get_words():
    start_time = time.time()
    buck_of_words = set()
    articles_processed = 0
    lines = 0
    modulo = 2

    with open('resources/wikipedia-lines.txt', 'r') as input_file, open('out/words.txt', 'w') as output_file:

        for i, line in enumerate(input_file):
            articles_processed = i / 2

            if i % 10000 == 0 and i != 0:
                print_time(start_time, articles_processed)

            if i % modulo == 1:
                for word in line.split():
                    if word not in buck_of_words:
                        buck_of_words.add(word)

        for word in sorted(buck_of_words):
            lines += 1
            output_file.write(word + '\n')

    print "Number of articles: ", (articles_processed / modulo) * 2, "Number of words: ", len(buck_of_words), "Number of lines: ", lines


get_words()