import time
import re

NUMBER_OF_ARTICLES = 4335341
EXCLUDED_WORDS = {'and', 'or', 'of', 'an', 'its', 'it', 'at', 'the', 'is', 'on', 'to', 'be', 'to',
         'for', 'do', '</doc>', 'but', 'as', 'by', 'in'}

BEGINNING_REGEX = re.compile("<doc\s+id=\"(\d+)\"\s*url=\"(.+)\"\s*title=\"(.+)\"\s*>")
NIELICZBY_REGEX = re.compile("\w*\d\w*")
ALFANUMERYCZNE_REGEX = re.compile("[^\w\s]")


def extract_words_from_line(line):
    """
        male litery, tylko alfanumeryczne znaki, nie EXCLUDED_WORDS, dluzsze niz 1 znak,
        nie slowa-myslniki
    """
    extracted_words = []

    line = re.sub("-", " ", line)
    line = re.sub("_", " ", line)
    line = NIELICZBY_REGEX.sub(" ", line)
    line = ALFANUMERYCZNE_REGEX.sub(" ", line).lower()

    for word in line.split():
        if len(word) > 1 and (word not in EXCLUDED_WORDS):
            extracted_words.append(word)

    return extracted_words


def print_time(start_time, articles_processed):
    difftime = time.time() - start_time
    fulltime = difftime * (1.0 * NUMBER_OF_ARTICLES / articles_processed)
    estimated_time = fulltime - difftime
    print '%d Estimated time: %dh %dm %ds' % (articles_processed, estimated_time / 3600, (estimated_time % 3600) / 60, estimated_time % 60)


def get_text_in_lines():
    start_time = time.time()
    articles_processed = 0
    article_words = []

    with open('resources/wikipedia-text.txt', 'r') as input_file, open('out/wikipedia-lines.txt', 'w') as output_file:

        for line in input_file:

            match_begin_doc = BEGINNING_REGEX.search(line)

            if articles_processed % 10000 == 0 and articles_processed != 0:
                    print_time(start_time, articles_processed)

            if match_begin_doc:
                article_id, article_url, article_name = match_begin_doc.groups()
                output_file.write(article_id + '|' + article_url + '|' + article_name + '\n')
                articles_processed += 1

            elif re.search('</doc>', line):
                for word in sorted(article_words):
                        output_file.write(word + ' ')
                output_file.write('\n')
                article_words = []

            else:
                article_words.extend(extract_words_from_line(line))

    print "Number of articles: ", articles_processed
