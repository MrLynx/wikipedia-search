import time
import re

NUMBER_OF_ARTICLES = 4335341


def get_articles(file_name):
    with open(file_name) as file:
        for i, line in enumerate(file):

            if i % 2 == 0:
                id, url, name = tuple(line.split('|'))

            else:
                yield id, url, name[:-1], line.split()


def print_time(start_time, articles_processed):
    difftime = time.time() - start_time
    fulltime = difftime * (1.0 * NUMBER_OF_ARTICLES / articles_processed)
    estimated_time = fulltime - difftime
    print '%d Estimated time: %dh %dm %ds' % (articles_processed, estimated_time / 3600, (estimated_time % 3600) / 60, estimated_time % 60)


def inverse_document_frequency():

    occurences = dict()
    with open('out/words-occurences.txt', 'w') as file:
        for i, line in enumerate(words_file):
            word = line[:-1]
            


inverse_document_frequency()