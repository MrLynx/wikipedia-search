import time

NUMBER_OF_ARTICLES = 4335341

def get_articles(file_name):
    with open(file_name) as file:
        for i, line in enumerate(file):

            if i % 2 == 0:
                id, url, name = tuple(line.split('|'))

            else:
                yield id, url, name[:-1], line.split()


def print_time(start_time, articles_processed):
    difftime = time.time() - start_time
    fulltime = difftime * (1.0 * NUMBER_OF_ARTICLES / articles_processed)
    estimated_time = fulltime - difftime
    print '%d Estimated time: %dh %dm %ds' % (articles_processed, estimated_time / 3600, (estimated_time % 3600) / 60, estimated_time % 60)


def get_urls():
    start_time = time.time()

    with open('out/urls.txt', 'w') as urls_file:
        for i, (id, url, name, words) in enumerate(get_articles('resources/wikipedia-lines.txt')):
            article_dict = dict()

            if i % 1000 == 0 and i != 0:
                print_time(start_time, i)

            # WRITE TO FILE
            urls_file.write(id + ' ' + url + ' ' + name + '\n')

get_urls()